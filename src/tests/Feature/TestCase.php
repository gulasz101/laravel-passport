<?php
declare(strict_types=1);

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Client;

/**
 * Class TestCase
 * @package Tests\Feature
 */
abstract class TestCase extends \Tests\TestCase
{
    use RefreshDatabase;

    /** @var User */
    protected $user;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('passport:keys');
        $this->artisan('passport:client', ['--password' => true, '--name' => config('app.name').' Password Grant Client'])->execute();

        $this->user = factory(User::class)->create(
            [
                'email' => 'email@example.com'
            ]
        );

        $client = Client::first();

        $_ENV['PASSPORT_CLIENT_ID'] = $client->id;
        $_ENV['PASSPORT_CLIENT_SECRET'] = $client->secret;

        $this->app->make('config')->set('services', require $this->app->configPath('services.php'));
    }
}

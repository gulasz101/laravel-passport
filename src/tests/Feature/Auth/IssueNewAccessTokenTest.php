<?php
declare(strict_types=1);

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Http\Response;
use Tests\Feature\TestCase;

/**
 * Class IssueNewAccessTokenTest
 * @package Tests\Feature\Auth
 */
class IssueNewAccessTokenTest extends TestCase
{
    public function testIssueAccessTokenPositive()
    {
        $this->withoutExceptionHandling();

        $response = $this->post(
            '/auth/login',
            [
                'username' => 'email@example.com',
                'password' => 'password'
            ]
        );

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('Bearer', \GuzzleHttp\json_decode($response->content())->token_type);
    }
}

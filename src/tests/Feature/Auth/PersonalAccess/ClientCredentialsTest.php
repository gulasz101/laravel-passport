<?php
declare(strict_types=1);

namespace Tests\Feature\Auth\PersonalAccess;

use App\User;
use function GuzzleHttp\json_decode;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Passport\Client;
use Tests\Feature\TestCase;

/**
 * Class PersonalAccessTokens
 * @package Tests\Feature\Auth\PersonalAccess
 */
class ClientCredentialsTest extends TestCase
{
    public function testIssueNewCCAccessToken()
    {
        $this->withoutExceptionHandling();
        $this->artisan(
            'passport:client',
            [
                '--name' => 'my name',
                '--redirect_uri' => 'http://localhost/auth/authorize',
                '--user_id' => $this->user->id
            ]
        );

        $client = Client::firstOrFail();

        $accessToken = json_decode(
            $this->postJson(
                '/auth/token',
                [
                    'grant_type' => 'client_credentials',
                    'client_id' => $client->id,
                    'client_secret' => $client->secret,
                    'scope' => ''
                ]
            )->content()
        );

        $this->assertEquals('Bearer', $accessToken->token_type);

        $r = $this->getJson('/api/user', ['Authorization' => $accessToken->token_type . ' ' . $accessToken->access_token]);
        dd($r->getStatusCode());
        $this->assertJson($this->user->toJson());
    }
}

<?php
declare(strict_types=1);

namespace Tests\Feature\Auth\AuthCode;

use function GuzzleHttp\json_decode;
use function GuzzleHttp\Psr7\parse_query;
use Illuminate\Http\Response;
use Laravel\Passport\AuthCode;
use Tests\Feature\TestCase;

/**
 * Class AuthCodeTest
 * @package Tests\Feature\Auth\AuthCode
 */
class AuthCodeTest extends TestCase
{
    public function testCreateNewClientForAuthorizationCodeUse()
    {
        $this->actingAs($this->user);

        $response = $this->postJson(
            '/auth/clients',
            [
                'name' => 'my client',
                'redirect' => 'http://localhost/auth/authorize'
            ]
        );

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($this->user->id, json_decode($response->content())->user_id);
    }

    public function testIssueNewAccessTokenWithAuthCodeGrantClient()
    {
        $this->withoutExceptionHandling();
        $this->actingAs($this->user);

        $client = json_decode(
            $this->postJson(
                '/auth/clients',
                [
                    'name' => 'my client',
                    'redirect' => 'http://localhost/auth/authorize'
                ]
            )->content()
        );

        $r = $this->getJson(
            '/auth/authorize?' . http_build_query(
                [
                    'client_id' => $client->id,
                    'redirect_uri' => $client->redirect,
                    'response_type' => 'code',
                    'scope' => '',
                ]
            )
        );


        $this->assertCount(0, AuthCode::all());

        $r = $this->post(
            '/auth/authorize',
            [
                'username' => $this->user->email,
                'password' => 'password',
            ]
        );

        $this->assertCount(1, AuthCode::all());

        $accessToken = json_decode(
            $this->postJson(
                '/auth/token',
                [
                    'grant_type' => 'authorization_code',
                    'client_id' => $client->id,
                    'client_secret' => $client->secret,
                    'redirect_uri' => $client->redirect,
                    'code' => parse_query(parse_url($r->headers->get('location'))['query'])['code'],
                ]
            )->content()
        );

        $this->assertEquals('Bearer', $accessToken->token_type);
    }
}